// Modules

module.exports = function (app) {
    var Route = app;

    Route.get('/', require('../controllers/index'));
    Route.all('/categories/:id?', require('../controllers/categories'));
};