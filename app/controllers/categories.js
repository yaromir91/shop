
module.exports = function (req, res, next) {
    switch(req.method){
        case 'GET' : index(); break;
        case 'POST' : post(); break;
        case 'PUT' : put(); break;
        default : next()
    }

    function index(){

        res.send({method : 'index', id : req.params.id || ''})
    }

    function post() {
        res.send({method : 'create', body : req.body || {}})
    };

    function put(){
        //res.send({method : 'create', put : req.body || put})
        next();
    }

};