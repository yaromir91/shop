var express    = require('express'),        // call express
    http = require('http'),
    path = require('path'),
    config = require('./app/config'),
    app        = express();                 // define our app using express


require('./libs/settings')(app, express);

http.createServer(app).listen(config.get('port'), function () {
    console.log('Init on port:' + config.get('port') );
});