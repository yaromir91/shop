var config = require('../app/config'),
    error = {};

error.sendHttpError = function (error, res) {
    res.send(error.status || 500, {error : error });
};

module.exports = error.errorsFunction = function(app, express) {
    // 404
    app.use(function (req, res, next) {
        var error = {
            desc : 'Not found',
            status : 404
        };

        next(error);
    });
    // other
    if(app.get('env') === 'development'){
        app.use(function (err, req, res, next) {
            error.sendHttpError(err, res)
        })
    }
};
