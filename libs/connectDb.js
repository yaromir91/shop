var mongoose = require('mongoose'),
    async = require('async'),
    config = require('../app/config');

function openConnection(cb) {
    mongoose.connection.on('open', function () {
        console.log('connected to database ' + config.get('db:name'));
        cb();
    });
}

function ensureIndexes(cb) {
    async.each(Object.keys(mongoose.models), function (model, callback) {
        mongoose.models[model].ensureIndexes(callback);
    }, function () {
        console.log('indexes ensured completely');
        cb();
    });
}

function closeConnection() {
    mongoose.disconnect();
    console.log('disconnected');
}

module.exports = function () {
    async.series(
        [
            openConnection,
            ensureIndexes
        ],
        closeConnection
    );
};