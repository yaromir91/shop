module.exports = function (app, express) {
    var path = require('path'),
        config = require('../app/config'),
        logger = require('morgan'),
        bodyParser = require('body-parser');
        mongoose = require('./connectDb'),
        //MongoStore = require('connect-mongo')(express),

        errorHandler = require('./error');

    /**
     * Logger
     * */
    if(app.get('env') == 'development'){
        app.use(logger('dev'));
    }


    /**
     * Session Params
     * */
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());

    /**
     * Route
     * */
    require('../app/routes')(app);

    /**
     * Error
     * */
    errorHandler(app, express);

};